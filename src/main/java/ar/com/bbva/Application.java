package ar.com.bbva;

import ar.com.bbva.application.usecase.InitializerUseCase;
import ar.com.bbva.application.usecase.ProcessDirUseCase;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Application {

    public static void main(String[] args) {
        InitializerUseCase starter = new InitializerUseCase();
        new ProcessDirUseCase(starter.getConfiguration()).process();
    }

}
