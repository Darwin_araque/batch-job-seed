package ar.com.bbva.config;

public final class AttributesConfig {

    public static final String SEMICOLON = ";";
    public static final Integer ID_CLIENT = 0;
    public static final Integer ADMIN = 1;
    public static final Integer CREDIT_ACCOUNT = 2;
    public static final Integer CLOSING_DATE = 3;
    public static final Integer REPORT_CODE = 4;
    public static final Integer FILE_NAME_GENERATE = 5;
    public static final Integer USER = 6;
    public static final Integer PASSWORD = 7;
    public static final Integer SUBSIDIARY = 8;
}
