package ar.com.bbva.config;

public final class ErrorCodeConfig {

    public static final String OK = "OK";
    public static final String EVENT_FILE_SUCCESSFUL_EXPORTED_J0000000 = "J0000000";
    public static final String EVENT_FILE_SUCCESSFUL_EXPORTED_MESSAGE = "El legajo se exportó correctamente";
    public static final String NOT_OK = "NO OK";
    public static final String EVENT_FILES_NOT_FOUND_IN_INPUT_FOLDER_CODE_J0000001 = "J0000001";
    public static final String EVENT_FILES_NOT_FOUND_IN_INPUT_FOLDER_CODE_MESSAGE = "No se encontraron archivos de entrada";
    public static final String EVENT_INPUT_FILE_EMPTY_CODE_J0000002 = "J0000002";
    public static final String EVENT_INPUT_FILE_EMPTY_CODE_MESSAGE = "El archivo de entrada está vacío.";
    public static final String EVENT_INPUT_FILE_NOT_FOUND_CODE_J0000008 = "J0000008";
    public static final String EVENT_INPUT_FILE_NOT_FOUND_CODE_MESSAGE = "No se pudo encontrar la carpeta de entrada";
    public static final String EVENT_IO_EXCEPTION_CODE_J000EXC1 = "J000EXC1";
    public static final String EVENT_IO_EXCEPTION_CODE_MESSAGE = "Ocurrió un error en Java al leer el archivo de entrada";
    public static final String EVENT_ERROR_REQUEST_PARAM_J000EXC2 = "J000EXC2";
    public static final String EVENT_ERROR_REQUEST_PARAM_MESSAGE = "Ocurrio un error en Java al exportar el PDF";
}

