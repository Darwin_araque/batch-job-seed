package ar.com.bbva.config;

public class PathConfig {

    public static String configFile = "config";
    public static String inPath = "entrada";
    public static String outPath = "salida";
    public static String processedPath = "procesados";
}
