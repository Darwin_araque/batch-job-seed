package ar.com.bbva.config;

public final class WSRestConfig {
    public static final String WS_REST_URL_MICROFIL = "http://dvi006-muleesb:8101/api/controld";
    public static final String WS_REST_URL_DOCUPRINT = "http://intra-services:8101/api/controld";
    public static final String RECIPIENT_DOCUPRINT = "docuprint";
    public static final String RECIPIENT_MICROFIL = "microfil";
    public static final String RECIPIENT_PARAM = "recipiente";
    public static final String REPORT_PARAM = "reporte";
    public static final String USER_PARAM = "usuario";
    public static final String PASSWORD_PARAM = "password";
    public static final String SUBSIDIARY_PARAM = "CASA";
    public static final String ACCOUNT_PARAM = "CUENTA";
    public static final String DATE_PARAM = "FECHA";
    public static final int MINUS_ONE = -1;
}
