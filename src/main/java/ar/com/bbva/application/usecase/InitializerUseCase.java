package ar.com.bbva.application.usecase;

import ar.com.bbva.config.ErrorCodeConfig;
import ar.com.bbva.config.PathConfig;
import ar.com.bbva.domain.LogMessage;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

@Slf4j
public class InitializerUseCase {

    private final Properties configuration;

    public InitializerUseCase() {
        this.configuration = new Properties();
        this.configure();
        this.buildWorkSpace();
    }

    public Properties getConfiguration() {
        return this.configuration;
    }

    private void configure() {

        File configFile = new File(PathConfig.configFile);
        try {
            FileReader reader = new FileReader(configFile);
            this.configuration.load(reader);
        } catch (FileNotFoundException fileNotFoundException) {
            this.configureWithDefaultValues();
        } catch (IOException ioException) {
            log.error(new LogMessage(null, null, null, null,
                    ErrorCodeConfig.NOT_OK, ErrorCodeConfig.EVENT_IO_EXCEPTION_CODE_J000EXC1,
                    ErrorCodeConfig.EVENT_IO_EXCEPTION_CODE_MESSAGE).toString());
        }

    }

    private void configureWithDefaultValues() {
        this.configuration.put(PathConfig.inPath, PathConfig.inPath);
        this.configuration.put(PathConfig.outPath, PathConfig.outPath);
        this.configuration.put(PathConfig.processedPath, PathConfig.processedPath);
    }

    private void buildWorkSpace() {
        this.configuration.stringPropertyNames().forEach(this::validateOrBuildDir);
    }

    private void validateOrBuildDir(String propertyPath) {
        File pathFile = new File(propertyPath);
        if (!pathFile.exists()) {
            new File(propertyPath).mkdir();
        }
    }
}
