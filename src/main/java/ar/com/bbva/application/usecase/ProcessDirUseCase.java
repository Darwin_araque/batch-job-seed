package ar.com.bbva.application.usecase;

import ar.com.bbva.config.AttributesConfig;
import ar.com.bbva.config.ErrorCodeConfig;
import ar.com.bbva.config.PathConfig;
import ar.com.bbva.config.WSRestConfig;
import ar.com.bbva.domain.LogMessage;
import ar.com.bbva.domain.Report;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Properties;

@Slf4j
public class ProcessDirUseCase {

    private final Properties configuration;

    public ProcessDirUseCase(Properties configuration) {
        this.configuration = configuration;
    }

    public void process() {
        this.validateDir();
        this.processFileInPath();
    }

    private void processFileInPath() {
        List<File> inPathfileList = new ArrayList<>(Arrays.asList(Objects.requireNonNull(
                new File(this.configuration.getProperty(PathConfig.inPath)).listFiles())));
        if (!inPathfileList.isEmpty()) {
            inPathfileList.forEach(this::processFiles);
            inPathfileList.forEach(inPathfile -> {
                try {
                    Files.move(Paths.get(inPathfile.getAbsolutePath()), Paths.get(this.configuration.getProperty(PathConfig.processedPath) + File.separator + inPathfile.getName()));
                    log.info(new LogMessage(null, null, null, null,
                            ErrorCodeConfig.OK, ErrorCodeConfig.EVENT_FILE_SUCCESSFUL_EXPORTED_J0000000, ErrorCodeConfig.EVENT_FILE_SUCCESSFUL_EXPORTED_MESSAGE).toString());
                } catch (IOException ioException) {
                    log.error(new LogMessage(null, null, null, null,
                            ErrorCodeConfig.NOT_OK, ErrorCodeConfig.EVENT_FILES_NOT_FOUND_IN_INPUT_FOLDER_CODE_J0000001,
                            ErrorCodeConfig.EVENT_FILES_NOT_FOUND_IN_INPUT_FOLDER_CODE_MESSAGE).toString());
                }
            });
        } else {
            log.error(new LogMessage(null, null, null, null,
                    ErrorCodeConfig.NOT_OK, ErrorCodeConfig.EVENT_INPUT_FILE_EMPTY_CODE_J0000002,
                    ErrorCodeConfig.EVENT_INPUT_FILE_EMPTY_CODE_MESSAGE).toString());
        }
    }

    private void processFiles(File inPathFile) {
        List<Report> reportList = new ArrayList<>();
        String line;
        try {
            FileReader fileReader = new FileReader(inPathFile);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            while ((line = bufferedReader.readLine()) != null) {
                Report report = this.covertLineInObjectReport(line);
                report.setRecipient(FilenameUtils.removeExtension(inPathFile.getName()));
                reportList.add(report);
            }
            bufferedReader.close();
            fileReader.close();
        } catch (FileNotFoundException fileNotFoundException) {
            log.error(new LogMessage(null, null, null, null,
                    ErrorCodeConfig.NOT_OK, ErrorCodeConfig.EVENT_FILES_NOT_FOUND_IN_INPUT_FOLDER_CODE_J0000001,
                    ErrorCodeConfig.EVENT_FILES_NOT_FOUND_IN_INPUT_FOLDER_CODE_MESSAGE).toString());
        } catch (IOException ioException) {
            log.error(new LogMessage(null, null, null, null,
                    ErrorCodeConfig.NOT_OK, ErrorCodeConfig.EVENT_IO_EXCEPTION_CODE_J000EXC1,
                    ErrorCodeConfig.EVENT_IO_EXCEPTION_CODE_MESSAGE).toString());
        }
        this.requestWsRest(reportList);
    }

    private void requestWsRest(List<Report> reportList) {
        CloseableHttpClient client = HttpClientBuilder.create().build();
        try {
            reportList.forEach(report -> {
                try {
                    HttpResponse response = client.execute(this.buildHttpGet(report));
                    BufferedInputStream bufferedInputStream = new BufferedInputStream(response.getEntity().getContent());
                    BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(
                            new FileOutputStream(this.configuration.getProperty(PathConfig.outPath) +
                                    File.separator + report.getFileNameGenerate()));
                    int inByte;
                    while ((inByte = bufferedInputStream.read()) != WSRestConfig.MINUS_ONE)
                        bufferedOutputStream.write(inByte);
                    bufferedInputStream.close();
                    bufferedOutputStream.close();
                } catch (IOException ioException) {
                    log.error(new LogMessage(null, null, null, null,
                            ErrorCodeConfig.NOT_OK, ErrorCodeConfig.EVENT_IO_EXCEPTION_CODE_J000EXC1,
                            ErrorCodeConfig.EVENT_IO_EXCEPTION_CODE_MESSAGE).toString());
                } catch (Exception requestParamRecipientException) {
                    log.error(requestParamRecipientException.getMessage());
                }
            });
            client.close();
        } catch (IOException ioException) {
            log.error(new LogMessage(null, null, null, null,
                    ErrorCodeConfig.NOT_OK, ErrorCodeConfig.EVENT_IO_EXCEPTION_CODE_J000EXC1,
                    ErrorCodeConfig.EVENT_IO_EXCEPTION_CODE_MESSAGE).toString());
        }
    }

    private HttpGet buildHttpGet(Report report) throws Exception {
        if (report.getRecipient().equalsIgnoreCase(WSRestConfig.RECIPIENT_DOCUPRINT)) {
            HttpGet httpGet = new HttpGet(WSRestConfig.WS_REST_URL_DOCUPRINT);
            httpGet.setURI(new URIBuilder(httpGet.getURI())
                    .addParameter(WSRestConfig.RECIPIENT_PARAM, report.getRecipient())
                    .addParameter(WSRestConfig.REPORT_PARAM, report.getReportCode())
                    .build());
            return httpGet;
        } else if (report.getRecipient().equalsIgnoreCase(WSRestConfig.RECIPIENT_MICROFIL)) {
            HttpGet httpGet = new HttpGet(WSRestConfig.WS_REST_URL_MICROFIL);
            httpGet.setURI(new URIBuilder(httpGet.getURI())
                    .addParameter(WSRestConfig.RECIPIENT_PARAM, report.getRecipient())
                    .addParameter(WSRestConfig.REPORT_PARAM, report.getReportCode())
                    .addParameter(WSRestConfig.USER_PARAM, report.getUser())
                    .addParameter(WSRestConfig.PASSWORD_PARAM, report.getPassword())
                    .addParameter(WSRestConfig.SUBSIDIARY_PARAM, report.getSubsidiary())
                    .addParameter(WSRestConfig.ACCOUNT_PARAM, report.getCreditAccount())
                    .addParameter(WSRestConfig.DATE_PARAM, report.getClosingDate())
                    .build());
            return httpGet;
        } else {
            throw new Exception(new LogMessage(null, null, null, null,
                    ErrorCodeConfig.NOT_OK, ErrorCodeConfig.EVENT_ERROR_REQUEST_PARAM_J000EXC2,
                    ErrorCodeConfig.EVENT_ERROR_REQUEST_PARAM_MESSAGE).toString());
        }
    }

    private Report covertLineInObjectReport(String line) {
        String[] lineAttributes = line.split(AttributesConfig.SEMICOLON);
        return new Report(null, lineAttributes[AttributesConfig.ID_CLIENT], lineAttributes[AttributesConfig.ADMIN],
                lineAttributes[AttributesConfig.CREDIT_ACCOUNT], lineAttributes[AttributesConfig.CLOSING_DATE],
                lineAttributes[AttributesConfig.REPORT_CODE], lineAttributes[AttributesConfig.FILE_NAME_GENERATE], null, null, null);
    }

    private void validateDir() {
        File inPathDirectory = new File(this.configuration.getProperty(PathConfig.inPath));
        File processedPathDirectory = new File(this.configuration.getProperty(PathConfig.processedPath));
        if (!inPathDirectory.exists()) {
            log.error(new LogMessage(null, null, null, null,
                    ErrorCodeConfig.NOT_OK, ErrorCodeConfig.EVENT_INPUT_FILE_NOT_FOUND_CODE_J0000008,
                    ErrorCodeConfig.EVENT_INPUT_FILE_NOT_FOUND_CODE_MESSAGE).toString());
        }
        if (!processedPathDirectory.exists()) {
            log.error(new LogMessage(null, null, null, null,
                    ErrorCodeConfig.NOT_OK, ErrorCodeConfig.EVENT_INPUT_FILE_NOT_FOUND_CODE_J0000008,
                    ErrorCodeConfig.EVENT_INPUT_FILE_NOT_FOUND_CODE_MESSAGE).toString());
        }
    }
}
