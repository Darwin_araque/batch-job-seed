package ar.com.bbva.domain;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;
import org.apache.commons.lang3.StringUtils;

public class LogMessage {

    private String date;
    private String clientId;
    private String operation;
    private String application;
    private String formDefinition;
    private String result;
    private String codError;
    private String message;

    public LogMessage(String clientId, String operation, String application, String formDefinition, String result,
            String codError, String message) {
        SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd;HH:mm:ss");
        this.date = formatDate.format(new Date());
        this.clientId = clientId;
        this.operation = operation;
        this.application = application;
        this.formDefinition = formDefinition;
        this.result = result;
        this.codError = codError;
        this.message = message;
    }


    public String toString() {
        return this.date + ";"
                + padString(this.clientId, 8) + ";"
                + padString(this.operation, 14) + ";"
                + padString(this.application, 30) + ";"
                + padString(this.result, 5) + ";"
                + padString(this.codError, 8) + ";"
                + padString(this.message, 200);

    }

    private String padString(String value, int size) {
        return Optional.ofNullable(value)
                .map(fullString -> StringUtils.rightPad(fullString, size))
                .orElse(StringUtils.rightPad(" ", size));
    }


}
