package ar.com.bbva.domain;

import lombok.Getter;
import lombok.Setter;

public class Report {

    @Getter
    @Setter
    private String recipient;
    @Getter
    @Setter
    private String idClient;
    @Getter
    @Setter
    private String admin;
    @Getter
    @Setter
    private String creditAccount;
    @Getter
    @Setter
    private String closingDate;
    @Getter
    @Setter
    private String reportCode;
    @Getter
    @Setter
    private String fileNameGenerate;
    @Getter
    @Setter
    private String user;
    @Getter
    @Setter
    private String password;
    @Getter
    @Setter
    private String subsidiary;

    public Report(String recipient, String idClient, String admin, String creditAccount, String closingDate, String reportCode, String fileNameGenerate, String user, String password, String subsidiary) {
        this.recipient = recipient;
        this.idClient = idClient;
        this.admin = admin;
        this.creditAccount = creditAccount;
        this.closingDate = closingDate;
        this.reportCode = reportCode;
        this.fileNameGenerate = fileNameGenerate;
        this.user = user;
        this.password = password;
        this.subsidiary = subsidiary;
    }
}
